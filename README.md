# Sdlloscope #

Sdlloscope is a soundcard oscilloscope written in SDL2. It currently supports only one channel.

### How do I compile? ###
You need:

* A C compiler such as gcc or clang
* The SDL2 development libraries:
	* SDL2
	* SDL2_ttf
* Make (not explicitly needed, can be built manually also)
