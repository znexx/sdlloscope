#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "main.h"
#include "gui.h"
#include "widgets.h"
#include "settings.h"

SDL_Window *window;
SDL_Renderer *renderer;
SDL_Texture *gui_texture;

TTF_Font *font;

int margin_left, margin_top, margin_right, margin_bottom;
int font_size = 12;

void render_text(SDL_Renderer *renderer, int x, int y, int horizontal_align, int vertical_align, const char* fmt, ...) {
	// format string
	char text_buffer[256];
	va_list args;
	va_start(args, fmt);
	vsnprintf(text_buffer, 256, fmt, args);
	va_end(args);

	// render text
	SDL_Color color = {195, 127, 219, SDL_ALPHA_OPAQUE};
	SDL_Surface* surface = TTF_RenderUTF8_Blended(font, text_buffer, color);

	// convert surface to texture and free surface
	SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_Rect target_rect = {x, y, surface->w, surface->h};
	SDL_FreeSurface(surface);

	// adjust positioning by alignment
	if (horizontal_align == 1) {
		target_rect.x -= target_rect.w;
	} else if (horizontal_align == 0) {
		target_rect.x -= target_rect.w / 2;
	}

	if (vertical_align == 1) {
		target_rect.y -= target_rect.h;
	} else if (vertical_align == 0) {
		target_rect.y -= target_rect.h / 2;
	}

	// actually render the texture
	SDL_RenderCopy(renderer, texture, NULL, &target_rect);
}

void GUI_Redraw (SDL_Renderer *renderer, Settings_t *settings) {
	SDL_SetRenderTarget(renderer, gui_texture);

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
	SDL_RenderClear(renderer);

	// zero line
	SDL_SetRenderDrawColor(renderer, 127, 127, 127, SDL_ALPHA_OPAQUE);
	SDL_RenderDrawLine(renderer, 0, float_to_y(0.0f), settings->window_width, float_to_y(0.0f));

	if (settings->draw_trigger) {
		// trigger index
		SDL_SetRenderDrawColor(
			renderer,
			0,
			127,
			0,
			SDL_ALPHA_OPAQUE
		);
		SDL_RenderDrawLine(
			renderer,
			settings->trigger_index,
			0,
			settings->trigger_index,
			settings->window_height
		);

		// trigger level
		SDL_SetRenderDrawColor(
			renderer,
			0,
			127,
			0,
			SDL_ALPHA_OPAQUE
		);

		SDL_RenderDrawLine(
			renderer,
			0,
			float_to_y(settings->trigger_level),
			settings->window_width,
			float_to_y(settings->trigger_level)
		);
	}

	if (settings->draw_grid) {
		for(float yf = -1.0f; yf <= 1.0f; yf += settings->grid_dy) {
			int y = float_to_y(yf);
			SDL_SetRenderDrawColor(renderer, 127, 127, 127, SDL_ALPHA_OPAQUE);
			for(int x = 0; x < settings->window_width; x += settings->grid_dx) {
				SDL_RenderDrawPoint(renderer, x, y);
			}
		}
	}

	Widgets_Render(renderer);

	render_text(renderer, margin_left, margin_top, -1, -1, "trigger level: %.02f", settings->trigger_level);
	render_text(renderer, margin_left, margin_top + font_size, -1, -1, "trigger index: %d", settings->trigger_index);
	render_text(renderer, margin_right, margin_top, 1, -1, "gain: %f", settings->gain);

	SDL_SetRenderTarget(renderer, NULL);
}

void GUI_Render (SDL_Renderer *renderer, Settings_t *settings) {
	if (settings->redraw) {
		SDL_Log("Redrawing GUI");
		Widgets_Setup(settings, margin_left, margin_top, margin_right, margin_bottom);
		GUI_Redraw(renderer, settings);

		settings->redraw = 0;
	}
	SDL_RenderCopy(renderer, gui_texture, NULL, NULL);
}

void GUI_LeftClick(int x, int y, Settings_t *settings) {
	Widgets_LeftClick(x, y , settings);
}

void GUI_Resize (SDL_Renderer *renderer, Settings_t *settings, int width, int height) {
	SDL_Log("Window resized");
	settings->redraw = 1;
	settings->window_width = width;
	settings->window_height = height;

	int margin = 20;

	margin_left = margin;
	margin_top = margin;
	margin_right = settings->window_width - margin;
	margin_bottom = settings->window_height - margin;

	Widgets_Setup(settings, margin_left, margin_top, margin_right, margin_bottom);

	SDL_DestroyTexture(gui_texture);
	gui_texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_UNKNOWN, SDL_TEXTUREACCESS_TARGET, settings->window_width, settings->window_height);
	SDL_SetTextureBlendMode(gui_texture, SDL_BLENDMODE_BLEND);
}

SDL_Renderer *GUI_Init (Settings_t *settings) {
	//SDL_CreateWindowAndRenderer(settings->window_width, settings->window_height, SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALWAYS_ON_TOP, &window, &renderer);
	SDL_CreateWindowAndRenderer(settings->window_width, settings->window_height, SDL_WINDOW_RESIZABLE, &window, &renderer);
	SDL_SetWindowTitle(window, "SDLLOSCOPE");
	SDL_GL_SetSwapInterval(1);
	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

	TTF_Init();
	font = TTF_OpenFont("amiga4ever pro2.ttf", font_size);
	if (font == NULL) {
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Font not found\n");
		exit(EXIT_FAILURE);
	}

	Widgets_Init(renderer);

	GUI_Resize(renderer, settings, settings->window_width, settings->window_height);

	return renderer;
}

void GUI_Quit () {
	Widgets_Quit();
	TTF_CloseFont(font);
	TTF_Quit();
	SDL_DestroyTexture(gui_texture);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
}
