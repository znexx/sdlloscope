#ifndef GUI_H
#define GUI_H

#include <SDL2/SDL.h>
#include "settings.h"

SDL_Renderer *GUI_Init (Settings_t *);
void GUI_Quit ();
void GUI_Render (SDL_Renderer*, Settings_t*);
void GUI_Resize (SDL_Renderer*, Settings_t*, int, int);
void GUI_LeftClick (int, int, Settings_t*);

#endif
