#include <SDL2/SDL.h>
#include <time.h>
#include <stdio.h>
#include "main.h"
#include "gui.h"
#include "settings.h"

#define DEBUG_SAMPLE_SOURCE
// 1 (pink)  are samples from the end of the last callback (n - trigger_index < 0)
// 2 (red)   are samples from this callback, before the trigger
// 3 (green) are samples from this callback, after the trigger
// 5 (teal)  are samples from callback with trigger in the last (no trigger in this callback)

#define INPUT_BUFFER_LENGTH 640
#define SCREEN_BUFFER_LENGTH 640
#define SAMPLING_RATE 96000

float y_zero;

float input_buffer[INPUT_BUFFER_LENGTH];
float screen_buffer[SCREEN_BUFFER_LENGTH];
int samplecolors[SCREEN_BUFFER_LENGTH];

int using_samples = 0;

SDL_AudioDeviceID dev;

Settings_t settings = {
	.window_width = 640,
	.window_height = 480,

	.please_sample_more = 1,
	.redraw = 1,

	.drawing_type = POINTS,

	.gain = 2.0f,

	.draw_overlay = 1,

	.draw_trigger = 1,
	.trigger_index = SCREEN_BUFFER_LENGTH / 2,

	.draw_grid = 1,
	.grid_dx = 16,
	.grid_dy = 0.1f,
};

int running = 1;
int paused = 0;

SDL_Color palette[] = {
	{255, 255, 255, 0}, // 0 white is normal sample
	{255,   0, 255, 0}, // 1 pink is clipping
	{255,   0,   0, 0}, // 2 red is above 0
	{  0, 255,   0, 0}, // 3 green is above 0
	{255, 255,   0, 0}, // 4 yellow is downwards zero transition
	{  0, 255, 255, 0}  // 5 teal is upwards zero transition
};

void audiocallback (void *userdata, Uint8 *stream, int len) {
	static int screen_buffer_index = 0;
	static int triggered = 0;
#ifdef DEBUG_SAMPLE_SOURCE
	static int c = 0;
#endif
	static float last_value = 0.0f;
	userdata = userdata;

	if (using_samples && !settings.please_sample_more) {
		return;
	}

	for(int n = 0; n < len/4; n++) {
		float new_value = ((float *)stream)[n];

		input_buffer[n] = new_value;

		if (triggered == 0 && last_value < settings.trigger_level && new_value > settings.trigger_level) {
			triggered = 1;

#ifdef DEBUG_SAMPLE_SOURCE
			c = 2;
#endif

			// fill screen buffer before trigger index
			int temp_n = n - settings.trigger_index;
			if (temp_n < 0) {
				temp_n += INPUT_BUFFER_LENGTH;
#ifdef DEBUG_SAMPLE_SOURCE
				c = 1;
#endif
			}
			screen_buffer_index = 0;
			while (screen_buffer_index < settings.trigger_index) {
				screen_buffer[screen_buffer_index] = input_buffer[temp_n];

#ifdef DEBUG_SAMPLE_SOURCE
				samplecolors[screen_buffer_index] = c;
#endif

				screen_buffer_index++;
				temp_n++;

				if (temp_n >= INPUT_BUFFER_LENGTH) {
					temp_n = 0;
#ifdef DEBUG_SAMPLE_SOURCE
					c = 2;
#endif
				}
			}
/*
			screen_buffer_index = settings.trigger_index;
			int old_n = n;
			while (screen_buffer_index > 0) {
				screen_buffer[screen_buffer_index] = input_buffer[n];

				screen_buffer_index--;
				n--;
				if (n < 0) {
					n += INPUT_BUFFER_LENGTH;
				}
			}
			n = old_n;
			screen_buffer_index = settings.trigger_index;
*/
#ifdef DEBUG_SAMPLE_SOURCE
			c = 3;
#endif
		}

		if (triggered) {
			screen_buffer[screen_buffer_index] = new_value;
#ifdef DEBUG_SAMPLE_SOURCE
			samplecolors[screen_buffer_index] = c;
#endif

			screen_buffer_index++;
			if (screen_buffer_index >= SCREEN_BUFFER_LENGTH) {
				settings.please_sample_more = 0;
				triggered = 0;
			}
		}

		last_value = new_value;
	}
#ifdef DEBUG_SAMPLE_SOURCE
	c = 5;
#endif
}

void color_data () {
#ifdef DEBUG_SAMPLE_SOURCE
	//return;
#endif
	for (int n = 0; n < SCREEN_BUFFER_LENGTH; n++) {
		samplecolors[n] = 0;

		if (screen_buffer[n] > settings.trigger_level) {
			if (n + 1 < SCREEN_BUFFER_LENGTH && screen_buffer[n + 1] < settings.trigger_level) {
				samplecolors[n] = 4;
			} else {
				samplecolors[n] = 2;
			}
		} else if (screen_buffer[n] < settings.trigger_level) {
			if (n + 1 < SCREEN_BUFFER_LENGTH && screen_buffer[n + 1] > settings.trigger_level) {
				samplecolors[n] = 5;
			} else {
				samplecolors[n] = 3;
			}
		}
	}
}

int float_to_y (float yf) {
	return y_zero - settings.gain*yf;
}

float y_to_float (int y) {
	return (y_zero - (float)y) / settings.gain;
}

void draw_points (SDL_Renderer *renderer) {
	for (int x = 0; x < SCREEN_BUFFER_LENGTH; x++) {
		SDL_Color *c = &palette[samplecolors[x]];
		SDL_SetRenderDrawColor(renderer, c->r, c->g, c->b, SDL_ALPHA_OPAQUE);
		SDL_RenderDrawPoint(renderer, x, float_to_y(screen_buffer[x]));
	}
}

void draw_lines (SDL_Renderer *renderer) {
	float y1, y2;
	for (int x = 0; x < SCREEN_BUFFER_LENGTH - 1; x++) {
		SDL_Color *c = &palette[samplecolors[x]];
		SDL_SetRenderDrawColor(renderer, c->r, c->g, c->b, SDL_ALPHA_OPAQUE);
		y1 = float_to_y(screen_buffer[x]);
		y2 = float_to_y(screen_buffer[x + 1]);
		SDL_RenderDrawLine(renderer, x, y1, x + 1, y2);
	}
}

void draw_bars (SDL_Renderer *renderer) {
	for (int x = 0; x < SCREEN_BUFFER_LENGTH; x++) {
		SDL_Color *c = &palette[samplecolors[x]];
		SDL_SetRenderDrawColor(renderer, c->r, c->g, c->b, SDL_ALPHA_OPAQUE);
		SDL_RenderDrawLine(renderer, x, y_zero, x, float_to_y(screen_buffer[x]));
	}
}

void draw_data (SDL_Renderer *renderer, Settings_t *settings) {
	switch(settings->drawing_type) {
		case POINTS:
		draw_points(renderer);
		break;
		case BARS:
		draw_bars(renderer);
		break;
		case LINES:
		draw_lines(renderer);
		break;
	}
}

void keydown (SDL_Keycode key) {
	switch (key) {
		case SDLK_ESCAPE:
			//running = 0;
			break;
		case SDLK_SPACE:
			paused = paused ^ 1;
			SDL_PauseAudioDevice(dev, paused);
			break;
	}
}

void keydown_shift (SDL_Keycode key) {
	switch (key) {
		case SDLK_UP:
			settings.trigger_level += 0.01f;
			SDL_Log("Trigger level: %.2f", settings.trigger_level);
			break;
		case SDLK_DOWN:
			settings.trigger_level -= 0.01f;
			SDL_Log("Trigger level: %.2f", settings.trigger_level);
			break;
		case SDLK_RIGHT:
			settings.trigger_index += 1;
			if (settings.trigger_index >= SCREEN_BUFFER_LENGTH) {
				settings.trigger_index = SCREEN_BUFFER_LENGTH - 1;
			}
			SDL_Log("Trigger index: %d", settings.trigger_index);
			break;
		case SDLK_LEFT:
			settings.trigger_index -= 1;
			if (settings.trigger_index < 0) {
				settings.trigger_index = 0;
			}
			SDL_Log("Trigger index: %d", settings.trigger_index);
			break;
	}
}

void keydown_ctrl (SDL_Keycode key) {
	switch (key) {
		case SDLK_b:
			settings.drawing_type = BARS;
			break;
		case SDLK_g:
			settings.draw_grid = settings.draw_grid ^ 1;
			break;
		case SDLK_l:
			settings.drawing_type = LINES;
			break;
		case SDLK_o:
			settings.draw_overlay = settings.draw_overlay ^ 1;
			break;
		case SDLK_p:
			settings.drawing_type = POINTS;
			break;
		case SDLK_t:
			settings.draw_trigger = settings.draw_trigger ^ 1;
			break;
		case SDLK_PLUS:
			settings.gain *= 1.25f;
			SDL_Log("Y scaling: %.2f", settings.gain);
			break;
		case SDLK_MINUS:
			settings.gain /= 1.25f;
			SDL_Log("Y scaling: %.2f", settings.gain);
			break;
	}
}

void keydown_shift_ctrl (SDL_Keycode key) {
	switch (key) {
		case SDLK_UP:
			settings.trigger_level += settings.grid_dy;
			SDL_Log("Trigger level: %.2f", settings.trigger_level);
			break;
		case SDLK_DOWN:
			settings.trigger_level -= settings.grid_dy;
			SDL_Log("Trigger level: %.2f", settings.trigger_level);
			break;
		case SDLK_RIGHT:
			settings.trigger_index += settings.grid_dx;
			if (settings.trigger_index >= SCREEN_BUFFER_LENGTH) {
				settings.trigger_index = SCREEN_BUFFER_LENGTH - 1;
			}
			SDL_Log("Trigger index: %d", settings.trigger_index);
			break;
		case SDLK_LEFT:
			settings.trigger_index -= settings.grid_dx;
			if (settings.trigger_index < 0) {
				settings.trigger_index = 0;
			}
			SDL_Log("Trigger index: %d", settings.trigger_index);
			break;
	}
}

int main (void) {
	SDL_Init(SDL_INIT_EVERYTHING);

	SDL_Renderer *renderer = GUI_Init(&settings);

	SDL_AudioSpec want, have;
	SDL_memset(&want, 0, sizeof(want));
	want.freq = SAMPLING_RATE;
	want.format = AUDIO_F32;
	want.channels = 1;
	want.samples = INPUT_BUFFER_LENGTH;
	want.callback = audiocallback;
	want.userdata = renderer;

	dev = SDL_OpenAudioDevice(NULL, 1, &want, &have, 0);
	if (dev == 0) {
		SDL_Log("failed to open audio: %s", SDL_GetError());
		running = 0;
	} else {
		SDL_Log("Successfully opened audio device");
		SDL_Log("Sampling frequency: %d", have.freq);
		SDL_Log("Sample format: %s", have.format == AUDIO_F32 ? "32 bit floating point" : "unknown");
		SDL_Log("Audio channels: %d", have.channels);
		SDL_Log("Sample buffer size: %d", have.samples);
		SDL_PauseAudioDevice(dev, 0);
	}

	y_zero = settings.window_height / 2;
	settings.gain = settings.window_height / 2.0f;

	SDL_Log("grid dx: %d samples (%.2f ms)", settings.grid_dx, 1000.0f*settings.grid_dx/SAMPLING_RATE);
	SDL_Log("grid dy: %.2f", settings.grid_dy);

	while (running) {
		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			SDL_Keymod mod;
			switch (event.type) {
				case SDL_QUIT:
					running = 0;
					break;
				case SDL_KEYDOWN:
					mod = event.key.keysym.mod & (KMOD_SHIFT | KMOD_CTRL);
					if (mod & KMOD_SHIFT && mod & KMOD_CTRL) {
						keydown_shift_ctrl(event.key.keysym.sym);
					} else if (mod & KMOD_SHIFT) {
						keydown_shift(event.key.keysym.sym);
					} else if (mod & KMOD_CTRL) {
						keydown_ctrl(event.key.keysym.sym);
					} else {
						keydown(event.key.keysym.sym);
					}
					settings.redraw = 1;
					break;
				case SDL_WINDOWEVENT:
					if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
						GUI_Resize(renderer, &settings, event.window.data1, event.window.data2);
						y_zero = settings.window_height / 2;
					}
					settings.redraw = 1;
					break;
				case SDL_MOUSEBUTTONDOWN:
					if (event.button.button == SDL_BUTTON_LEFT) {
						GUI_LeftClick(event.button.x, event.button.y, &settings);
					}
					break;
			}
		}

		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
		SDL_RenderClear(renderer);

		using_samples = 1;
		color_data();
		draw_data(renderer, &settings);
		using_samples = 0;

		GUI_Render(renderer, &settings);

		SDL_RenderPresent(renderer);
		settings.please_sample_more = 1;
	}

	GUI_Quit();

	SDL_CloseAudioDevice(dev);
	SDL_Quit();

	return 0;
}
