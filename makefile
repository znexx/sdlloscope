SHELL=/bin/sh
CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra -Og -g
LDFLAGS=-L. -lSDL2 -lSDL2_ttf -lm -pedantic -Wall -Wextra

SRC=$(wildcard *.c)
OBJ=$(SRC:.c=.o)
BIN=$(notdir $(shell pwd))

.PHONY: clean $(TESTFILE)

$(BIN): $(OBJ)
	$(CC) -o $@ $(OBJ) $(LDFLAGS)

run: $(BIN)
	./$(BIN)

test: $(BIN) $(TESTFILE)
	./$(BIN) $(TESTFILE)

clean:
	rm -rf $(BIN) $(OBJ)
