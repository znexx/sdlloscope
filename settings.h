#ifndef SETTINGS_H
#define SETTINGS_H

typedef enum {
	NONE,
	ONCE,
	CONTINUOUS
} Trigger_mode_t;

typedef enum {
	POSITIVE,
	NEGATIVE,
	BOTH,
} Trigger_slope_t;

typedef enum {
	POINTS,
	BARS,
	LINES
} Drawing_type_t;

typedef struct {
	int window_width;
	int window_height;
	int redraw;
	int please_sample_more;

	Drawing_type_t drawing_type;

	float gain;

	int draw_overlay;

	int draw_trigger;
	Trigger_mode_t trigger_mode;
	Trigger_slope_t trigger_slope;
	float trigger_level;
	int trigger_index;

	int draw_grid;
	int grid_dx;
	float grid_dy;
} Settings_t;

#endif
