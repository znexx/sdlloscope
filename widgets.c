#include <SDL2/SDL.h>
#include "widgets.h"
#include "settings.h"

#define N_WIDGETS 3

int widget_size = 48;
int icon_size;
SDL_Texture *icons;

typedef struct Widget_struct Widget_t;

struct Widget_struct {
	SDL_Rect rect;
	int icon_number;
	void (*onclick)(Widget_t *, Settings_t *);
};

Widget_t widgets[N_WIDGETS];

void onclick_toggle_trigger (Widget_t *self, Settings_t *settings) {
	self->icon_number = settings->draw_trigger;
	settings->draw_trigger = settings->draw_trigger ^ 1;
}

void onclick_toggle_grid (Widget_t *self, Settings_t *settings) {
	self->icon_number = settings->draw_grid;
	settings->draw_grid = settings->draw_grid ^ 1;
}

void onclick_switch_drawing_type (Widget_t *self, Settings_t *settings) {
	switch(settings->drawing_type) {
		case LINES:
		settings->drawing_type = BARS;
		break;
		case BARS:
		settings->drawing_type = POINTS;
		break;
		case POINTS:
		settings->drawing_type = LINES;
		break;
	}
	self->icon_number = 5 + settings->drawing_type;
}

void Widgets_Render (SDL_Renderer *renderer) {
	SDL_SetRenderDrawColor(renderer, 255, 0, 255, SDL_ALPHA_OPAQUE);
	SDL_Rect src_rect = {0, 0, icon_size, icon_size};
	for(int n = 0; n < N_WIDGETS; n++) {
		src_rect.x = icon_size * widgets[n].icon_number;
		SDL_RenderCopy(renderer, icons, &src_rect, &widgets[n].rect);
	}

}

void Widgets_LeftClick (int x, int y, Settings_t *settings) {
	SDL_Log("Mouse clicked at %d,%d", x, y);
	SDL_Point p = {x, y};
	for (int n = 0; n < N_WIDGETS; n++) {
		if (SDL_PointInRect(&p, &widgets[n].rect)) {
			widgets[n].onclick(&widgets[n], settings);
		}
	}
}

void Widgets_Setup (Settings_t *settings, int margin_left, int margin_top, int margin_right, int margin_bottom) {
	widgets[0].rect = (SDL_Rect){margin_left, margin_top + 3 * widget_size, widget_size, widget_size};
	widgets[0].icon_number = settings->draw_trigger;
	widgets[0].onclick = &onclick_toggle_trigger;

	widgets[1].rect = (SDL_Rect){margin_left, margin_top + 4 * widget_size, widget_size, widget_size};
	widgets[1].icon_number = 2 + settings->draw_grid;
	widgets[1].onclick = &onclick_toggle_grid;

	widgets[2].rect = (SDL_Rect){margin_right - widget_size, margin_bottom - widget_size, widget_size, widget_size};
	widgets[2].icon_number = 5 + settings->drawing_type;
	widgets[2].onclick = &onclick_switch_drawing_type;
}

void Widgets_Init (SDL_Renderer *renderer) {
	SDL_Surface *icons_surface = SDL_LoadBMP("icons.bmp");
	icons = SDL_CreateTextureFromSurface(renderer, icons_surface);
	icon_size = icons_surface->h;
	SDL_FreeSurface(icons_surface);
}

void Widgets_Quit () {
	SDL_DestroyTexture(icons);
}
