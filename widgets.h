#ifndef WIDGETS_H
#define WIDGETS_H

#include <SDL2/SDL.h>
#include "settings.h"

void Widgets_Render (SDL_Renderer *);
void Widgets_LeftClick (int, int, Settings_t *);
void Widgets_Setup (Settings_t *, int, int, int, int);
void Widgets_Init (SDL_Renderer *);
void Widgets_Quit ();

#endif
